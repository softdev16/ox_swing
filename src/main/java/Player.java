/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author NitroEz
 */
public class Player {
    private char player;
    private boolean win;
    private boolean lose;
    private boolean draw;
    private int nWin;
    private int nDraw;
    private int nLose;

    public Player(char x) {
        this.player = x;
        this.win = false;
        this.lose = false;
        this.draw = false;
        this.nWin=0;
        this.nLose=0;
        this.nDraw=0;
    }

    /*Player(char c) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/

    public char getPlayer() {
        return player;
    }

    public void setPlayer(char player) {
        this.player = player;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public boolean isLose() {
        return lose;
    }

    public void setLose(boolean lose) {
        this.lose = lose;
    }

    public boolean isDraw() {
        return draw;
    }

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

    public void win() {
        win = true;
    }

    public void draw() {
        draw = true;
    }

    public void lose() {
        lose = true;
    }
    
    public void upWin(){
        nWin++;
    }
    public void upDraw(){
        nDraw++;
    }
    public void upLose(){
        nLose++;
    }
    
    public int getNWin(){
        return nWin;
    }
    public int getNDraw(){
        return nDraw;
    }
    public int getNLose(){
        return nLose;
    }
}

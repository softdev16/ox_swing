/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author NitroEz
 */
public class Table {
    private char[][] table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };

    private Player playerX;
    private Player playerO;
    private Player playerWinner;
    private boolean finish = false;
    private int round;
    private int lastrow, lastcol;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
        round = 1;
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public int getRound() {
        return round;
    }

    public Player getTurn() {
        if (this.getRound() % 2 == 1)
            return playerX;
        return playerO;
    }

    public int switchPlayer() {
        return round++;
    }

    public boolean checkTable(int row, int col) {
        if (this.getTable()[row][col] == '-')
            return true;
        return false;
    }

    public void setTable(int row, int col) {
        this.getTable()[row][col] = getTurn().getPlayer();
        lastrow = row;
        lastcol = col;
    }

    public char[][] getTable() {
        return table;
    }

    public void checkWin() {
        checkCol();
        checkRow();
        checkX1();
        checkX2();
        if(getRound()==9|| (CheckTableDraw()==true && isFinish() == false)){
            setFinish();
//            System.out.println("Finish");
        }
    }

    public void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != getTurn().getPlayer()) {
                return;
            }
            // System.out.println("1: "+table[lastrow][col]);
            // System.out.println("2: "+getTurn().getPlayer());
        }
        finish = true;
        playerWinner = getTurn();
        setStatWinLose();
    }

    public void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != getTurn().getPlayer()) {
                return;
            }
        }
        finish = true;
        playerWinner = getTurn();
        setStatWinLose();
    }

    public void checkX1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != getTurn().getPlayer()) {
                return;
            }
        }
        finish = true;
        playerWinner = getTurn();
        setStatWinLose();
    }

    public void checkX2() {
        int colx = 2;
        for (int i = 0; i < 3; i++) {
            if (table[i][colx] != getTurn().getPlayer()) {
                return;
            }
            colx--;
        }
        finish = true;
        playerWinner = getTurn();
        setStatWinLose();
    }

    public void setStatWinLose() {
        if (getTurn() == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    public boolean isFinish() {
        return finish;
    }

    public void setFinish() {
        finish = true;
    }

    public Player getWinner() {
        return playerWinner;
    }

    private boolean CheckTableDraw() {
        int x=0;
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][j]!='-')
                    x++;
                else
                    break;
            }
        }
        
        if(x==9){
            return true;
        }else{
            return false;
        }
    }
    
}
